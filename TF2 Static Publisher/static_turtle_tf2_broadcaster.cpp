#include <chrono>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <utility>

#include "lifecycle_msgs/msg/transition.hpp"
#include "lifecycle_msgs/msg/transition_event.hpp"

#include "rclcpp/rclcpp.hpp"
#include "rclcpp/publisher.hpp"

#include "rclcpp_lifecycle/lifecycle_node.hpp"
#include "rclcpp_lifecycle/lifecycle_publisher.hpp"
#include "nav2_util/lifecycle_node.hpp"

#include "rcutils/logging_macros.h"

#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/quaternion.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "nav_msgs/msg/odometry.hpp"

#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Transform.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"

#include <rclcpp/clock.hpp>
#include <rclcpp/time.hpp>


using namespace std::chrono_literals;

std::string static_turtle_name;

class MinimalPublisher : public rclcpp::Node
{
  public:
    MinimalPublisher()
    : Node("learning_tf2")
    { 
     publisher_ = this->create_publisher<nav_msgs::msg::Odometry>("topic",10);
     timer_ = this->create_wall_timer(500ms, std::bind(&MinimalPublisher::timer_callback, this));
    }

  private:
    void timer_callback()
    {
      //std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<nav_msgs::msg::Odometry>> publisher_;
      //std::shared_ptr<rclcpp::TimerBase> timer_;
      
      static_turtle_name = "Wheels";

      geometry_msgs::msg::TransformStamped static_transformStamped;

      static_broadcaster_ = std::make_unique<tf2_ros::StaticTransformBroadcaster>(this);

      //static_transformStamped.header.stamp = rclcpp::Time::now();
      static_transformStamped.header.frame_id = "world";
      static_transformStamped.child_frame_id = static_turtle_name;
      static_transformStamped.transform.translation.x = 0.0;
      static_transformStamped.transform.translation.y = 0.0;
      static_transformStamped.transform.translation.z = 1.0;
      
      tf2::Quaternion q;
        q.setRPY(0, 0, 0);
      geometry_msgs::msg::Quaternion quat_msg;
      quat_msg = toMsg(q);
      
      static_transformStamped.transform.rotation = quat_msg;
      static_broadcaster_->sendTransform(static_transformStamped);
      
      nav_msgs::msg::Odometry odom;
      
      //odom.header.stamp = static_transformStamped.header.stamp;
      odom.header.frame_id = static_transformStamped.header.frame_id;
      odom.child_frame_id = static_turtle_name;
      odom.pose.pose.position.x = static_transformStamped.transform.translation.x;
      odom.pose.pose.position.y = static_transformStamped.transform.translation.y;
      odom.pose.pose.position.z = static_transformStamped.transform.translation.z;
      odom.pose.pose.orientation = quat_msg;

      RCLCPP_INFO(get_logger(),"Publishing: '%s'", static_turtle_name.c_str());
      
      publisher_->publish(std::move(odom));
    }
    rclcpp::TimerBase::SharedPtr timer_;  
    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr publisher_;
    std::unique_ptr<tf2_ros::StaticTransformBroadcaster> static_broadcaster_;
};  

int main(int argc, char * argv[])
  {
    rclcpp::init(argc,argv);
    rclcpp::spin(std::make_shared<MinimalPublisher>());
    rclcpp::shutdown();
    return 0;
  }
