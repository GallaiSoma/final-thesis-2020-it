from  launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='realsense_node',
            node_namespace='',
            node_executable='realsense_node',
            node_name='realsense'
),
        Node(
            package='t265_package',
            node_namespace='',
            node_executable='listener',
            node_name='t265'
)])
